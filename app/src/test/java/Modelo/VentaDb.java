package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.armenta.examencorte2.Ventas;
import java.util.ArrayList;

public class VentaDb implements Persistencia, Proyeccion {
    private Context context;
    private VentasDbHelper helper;
    private SQLiteDatabase db;

    public VentaDb(Context context, VentasDbHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public VentaDb(Context context) {
        this.context = context;
        this.helper = new VentasDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertVenta(Ventas venta) {
        ContentValues values = new ContentValues();
        values.put(DefineTabla.Ventas.COLUMN_NAME_NUM_BOMBA, venta.getNumBomba());
        values.put(DefineTabla.Ventas.COLUMN_NAME_TIPO_GASOLINA, venta.getTipoGasolina());
        values.put(DefineTabla.Ventas.COLUMN_NAME_PRECIO_GASOLINA, venta.getPrecioGasolina());
        values.put(DefineTabla.Ventas.COLUMN_NAME_CANTIDAD_GASOLINA, venta.getCantidadGasolina());
        values.put(DefineTabla.Ventas.COLUMN_NAME_TOTAL_PA, venta.getTotalpa());

        this.openDataBase();
        long num = db.insert(DefineTabla.Ventas.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar", "insertVenta: " + num);

        return num;
    }

    @Override
    public Ventas getVenta(int numBomba) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Ventas.TABLE_NAME,
                DefineTabla.Ventas.REGISTRO,
                DefineTabla.Ventas.COLUMN_NAME_NUM_BOMBA + " = ?",
                new String[]{String.valueOf(numBomba)},
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            Ventas venta = readVenta(cursor);
            cursor.close();
            return venta;
        }
        return null;
    }

    @Override
    public ArrayList<Ventas> allVentas() {
        this.openDataBase(); // Abre la base de datos

        Cursor cursor = db.query(
                DefineTabla.Ventas.TABLE_NAME,
                DefineTabla.Ventas.REGISTRO,
                null, null, null, null, null);
        ArrayList<Ventas> ventas = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            Ventas venta = readVenta(cursor);
            ventas.add(venta);
            cursor.moveToNext();
        }

        cursor.close();

        this.closeDataBase();
        return ventas;
    }

    @Override
    public Ventas readVenta(Cursor cursor) {
        Ventas venta = new Ventas();
        venta.setNumBomba(cursor.getInt(0));
        venta.setTipoGasolina(cursor.getInt(1));
        venta.setPrecioGasolina(cursor.getFloat(2));
        venta.setCantidadGasolina(cursor.getFloat(3));
        venta.setTotalpa(cursor.getFloat(4));
        return venta;
    }
}

