package Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class VentasDbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String FLOAT_TYPE = " REAL";
    private static final String COMMAT_SEP = " ,";
    private static final String SQL_CREATE_VENTAS = "CREATE TABLE " +
            DefineTabla.Ventas.TABLE_NAME + " (" +
            DefineTabla.Ventas.COLUMN_NAME_NUM_BOMBA + INTEGER_TYPE + " PRIMARY KEY" + COMMAT_SEP +
            DefineTabla.Ventas.COLUMN_NAME_TIPO_GASOLINA + INTEGER_TYPE + COMMAT_SEP +
            DefineTabla.Ventas.COLUMN_NAME_PRECIO_GASOLINA + FLOAT_TYPE + COMMAT_SEP +
            DefineTabla.Ventas.COLUMN_NAME_CANTIDAD_GASOLINA + FLOAT_TYPE + COMMAT_SEP +
            DefineTabla.Ventas.COLUMN_NAME_TOTAL_PA + FLOAT_TYPE + ")";
    private static final String SQL_DELETE_VENTAS = "DROP TABLE IF EXISTS " +
            DefineTabla.Ventas.TABLE_NAME;
    private static final String DATABASE_NAME = "sistema9.db";
    private static final int DATABASE_VERSION = 2;

    public VentasDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_VENTAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_VENTAS);
        onCreate(db);
    }
}


