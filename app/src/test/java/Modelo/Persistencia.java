package Modelo;

import android.database.Cursor;

import com.armenta.examencorte2.Ventas;
import java.util.ArrayList;

public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertVenta(Ventas venta);

}

