package Modelo;

import android.database.Cursor;

import com.armenta.examencorte2.Ventas;
import java.util.ArrayList;

public interface Proyeccion {
    Ventas getVenta(int numBomba);
    ArrayList<Ventas> allVentas();
    Ventas readVenta(Cursor cursor);
}
