package com.armenta.examencorte2;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.VentaDb;
import com.armenta.examencorte2.Ventas;
public class Aplicacion extends Application {
    public static ArrayList<Ventas> ventas;
    private MiAdaptador adaptador;
    private VentaDb ventaDb;

    public ArrayList<Ventas> getVentas() {
        return ventas;
    }

    public MiAdaptador getAdaptador() {
        return adaptador;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ventaDb = new VentaDb(getApplicationContext());
        ventas = ventaDb.allVentas();
        ventaDb.openDataBase();

        Log.d("", "onCreate: Tamaño del ArrayList: " + ventas.size());
        this.adaptador = new MiAdaptador(ventas, this);
    }

    public void agregarVenta(Ventas venta) {
        ventaDb.openDataBase();
        long resultado = ventaDb.insertVenta(venta);
        if (resultado != -1) {
            venta.setNumBomba((int) resultado);
            ventas.add(venta);
            adaptador.notifyDataSetChanged();
        }
    }
}
